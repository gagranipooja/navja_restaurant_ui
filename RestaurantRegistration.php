<html>
    <head>
        <title></title>
        <style>
            body {
                background-image: url("close-up-1854245_1920.jpg");
                /*                close-up-1854245_1920.jpg");*/
                background-color: #cccccc;
                background-position: center; /* Center the image */
                background-repeat: no-repeat; /* Do not repeat the image */
                background-size: cover;
            }
            form {
                text-align: left;
                margin: 0 auto;
                margin-top: 20px;
                /* color: green; */
            }
            input[type=text], input[type=password], input[type=email] {
                /*                margin-left: 100px;
                                display: table-cell;*/
                padding-left: 10px;
                width: 250px;
                height: 30px;

            }
            input[type=submit]{
                /*                margin-left: 140px;*/
                width: 80px;
                height: 30px;
            }
            b {
                margin-left: 100px;
            }    
            label {
                float:left;
                width: 10%;
                text-align:left;
            }
            p
            {
                /* text-align:left;*/
                margin-left: 100px;
            }
            .topnav {
                overflow: hidden;
                background-color: #39d07f; /* f3f3f3 009900*/
            }
            .topnav a {
                float: left;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }
            .topnav a:hover {
                text-decoration: underline;
            }
            .topnav a.active {
                background-color: #4CAF50;
                color: white;
            }
            .restaurantname {
                font-size: 50px; 
                color:#cc0033; 
                margin-left: 70px; 
                margin-top: 50px;
            }
        </style>
    </head>
    <body>      
        <div class="topnav" style="width:90%; margin: auto;">
            <a href="/PhpProjectLessons/RestaurantHomePage.php"><img src="LogoOne.png" alt="Restaurant Logo" height="60" width="60"/></a>
            <a href="/PhpProjectLessons/RestaurantOrderMenu.php" style="float:right">Order Now</a>
            <a href="#ContactUs" style="float:right">Contact Us</a>
            <a href="/PhpProjectLessons/RestaurantRegistration.php" style="float:right">Sign Up</a>
            <a href="/PhpProjectLessons/LoginPage.php" style="float:right">Login</a> 
            <a style="text-decoration: none;" href="/PhpProjectLessons/RestaurantHomePage.php"><b class="restaurantname">Navja Restaurant</b></a>
        </div> <br>
        <h3 style="margin-left: 100px;">Create Your Account</h3>
        <form action="RestaurantRegistration.php" method="post">
            <p><label>First Name:</label> 
                <input type="text" name="FirstName" required>
            </p>
            <p><label>Last Name:</label>   
                <input type="text" name="LastName" required>
            </p>
            <p><label>Email:</label>   
                <input type="email" name="email" required>
            </p>
            <p><label>Password:</label>   
                <input type="password" name="password" required>
            </p>
            <p><label>Confirm Password:</label>   
                <input type="password" name="confirmpassword" required>
            </p>
            <p><label>Phone Number:</label>   
                <input type="text" name="phone" required>
            </p>
            <p><label>Gender:</label>   
                <input type="radio" name="gender" value="male">Male
                <input type="radio" name="gender" value="female">Female
            </p>
            <p><input type="submit" value="Create Account" style="width: 150px;background-color:gray;color:white"></p>
        </form>
    </body>
</html>