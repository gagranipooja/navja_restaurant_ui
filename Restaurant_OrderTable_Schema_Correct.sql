--
-- Creating database
--

SET @OLD_UNIQUE_CHECKS=@@UNIQuE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS Restaurantproject;
CREATE SCHEMA Restaurantproject;

-- Create Food_Menu table
CREATE TABLE Restaurantproject.FoodMenu
(
  Food_Id INT(5) NOT NULL AUTO_INCREMENT,
  FoodName VARCHAR(30) NOT NULL,
  Food_Description VARCHAR(80) NOT NULL,
  Price VARCHAR(10) NOT NULL,
  PRIMARY KEY  (Food_Id)
) AUTO_INCREMENT=1
ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create DeliveryPerson table
CREATE TABLE Restaurantproject.DeliveryPerson
(
  Delivery_Person_Id INT(5) NOT NULL AUTO_INCREMENT,
  DeliveryPersonName VARCHAR(30) NOT NULL,
  Contact_Info CHAR(10) NOT NULL,
  Assigned_AreaCode INT(10) NOT NULL,
  PRIMARY KEY  (Delivery_Person_Id)
) AUTO_INCREMENT=1
ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create ordertable table. This is the main table of restaurant project.
CREATE TABLE Restaurantproject.Order
(
  Order_Id INT(5) NOT NULL AUTO_INCREMENT,
  Customer_FirstName VARCHAR(30) NOT NULL,
  Customer_LastName VARCHAR(30),
  Customer_PhoneNo CHAR(20) NOT NULL,
  Customer_EmailAddress VARCHAR(40) NOT NULL,
  Customer_ZipCode INT(10) NOT NULL,
  Customer_DeliveryAddress VARCHAR(40) NOT NULL,
  Food_Id INT(10) NOT NULL,
  Food_Quantity INT(10) NOT NULL,
  Total_Price VARCHAR(10) NOT NULL,
  Delivery_Person_Id INT(10),
  Name_On_Card VARCHAR(30),
  Card_Number VARCHAR(20),
  Expiration_Date VARCHAR(30),
  Order_Date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (Order_Id,Food_Id),
  CONSTRAINT fk_order_FoodId FOREIGN KEY (Food_Id) REFERENCES FoodMenu(Food_Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT fk_order_DeliverPersonId FOREIGN KEY (Delivery_Person_Id) REFERENCES DeliveryPerson(Delivery_Person_Id) ON DELETE RESTRICT ON UPDATE CASCADE
) AUTO_INCREMENT=1
ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create Registration table
CREATE TABLE Restaurantproject.Registration (
  user_id INT(5) NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  email_id VARCHAR(50) NOT NULL,
  password VARCHAR(20) NOT NULL,
  gender VARCHAR(10) NOT NULL,
  PRIMARY KEY  (user_id)
) AUTO_INCREMENT=1
ENGINE=InnoDB DEFAULT CHARSET=utf8;