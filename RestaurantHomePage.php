<html>
    <head>
        <title>Order Information</title>
        <style>
            body {
                background-image: url("close-up-1854245_1920.jpg"); 
                background-color: DCDCDC;
                background-position: center; /* Center the image */
                background-repeat: no-repeat; /* Do not repeat the image */
                background-size: cover;
            }
            .topnav {
                overflow: hidden;
                background-color: #39d07f; /* f3f3f3 009900*/
            }
            .topnav a {
                float: left;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }
            .topnav a:hover {
                text-decoration: underline;
            }
            .topnav a.active {
                background-color: #4CAF50;
                color: white;
            }
            .restaurantname {
                font-size: 50px; 
                color:#cc0033; 
                margin-left: 70px; 
                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <div class="topnav" style="width:90%; margin: auto;">
            <a href="/PhpProjectLessons/RestaurantHomePage.php"><img src="LogoOne.png" alt="Restaurant Logo" height="60" width="60"/></a>
            <a href="/PhpProjectLessons/RestaurantOrderMenu.php" style="float:right">Order Now</a>
            <a href="#ContactUs" style="float:right">Contact Us</a>
            <a href="/PhpProjectLessons/RestaurantRegistration.php" style="float:right">Sign Up</a>
            <a href="/PhpProjectLessons/LoginPage.php" style="float:right">Login</a> 
            <a style="text-decoration: none;" href="/PhpProjectLessons/RestaurantHomePage.php"><b class="restaurantname">Navja Restaurant</b></a>
        </div>  
        <div style="text-align:left; margin-top: 120px;">
            <span style="color: #cc0000; margin-left: 60px; margin-top: 200px; font-size: 20px; font-weight: 50; font-family: Sans-serif">
                HEALTHY SALADS, SANDWICHES AND BEVERAGES</span><br><br>
            <span  style="color: #cc0000;  margin-left: 120px; font-weight: 50; font-size: 18px; font-family: Sans-serif"> 
                DELIVERED TO YOUR DOORSTEP
            </span><br><br><br>
        </div><br><br>
        <div>
            <a style="margin-left: 200px; background-color: #39d07f; color: white; padding: 14px 25px; text-align: center; text-decoration: none;" href="RestaurantOrderInformation.php" target="_self">ORDER NOW</a><br><br><br><br>
            <span style="color: green;  margin-left: 80px; font-weight: 50; font-size: 18px; font-family: Sans-serif">| MINIMUM ORDER  $10     |     10:00 AM TO 10:00 PM |</span>
        </div>
    </body>
</html>