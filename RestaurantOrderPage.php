<html>
    <head>
        <title>Order Information</title>
        <style>
            body {
                /* background-image: url("wood-2142241_1920.jpg"); */
                background-color: whitesmoke;  /*DCDCDC*/
                background-position: center; /* Center the image */
                background-repeat: no-repeat; /* Do not repeat the image */
                background-size: cover;
            }
            .topnav {
                overflow: hidden;
                background-color: #39d07f; /* f3f3f3 009900*/
            }
            .topnav a {
                float: left;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }
            .topnav a:hover {
                text-decoration: underline;
            }
            .topnav a.active {
                background-color: #4CAF50;
                color: white;
            }
            .restaurantname {
                font-size: 50px; 
                color:#cc0033; 
                margin-left: 70px; 
                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <div class="topnav" style="width:90%; margin: auto;">
            <a href="/PhpProjectLessons/RestaurantHomePage.php"><img src="LogoOne.png" alt="Restaurant Logo" height="60" width="60"/></a>
            <a href="/PhpProjectLessons/RestaurantOrderPage.php" style="float:right">Order Now</a>
            <a href="#ContactUs" style="float:right">Contact Us</a>
            <a href="/PhpProjectLessons/RestaurantRegistration.php" style="float:right">Sign Up</a>
            <a href="/PhpProjectLessons/LoginPage.php" style="float:right">Login</a> 
            <a style="text-decoration: none;" href="/PhpProjectLessons/RestaurantHomePage.php"><b class="restaurantname">Navja Restaurant</b></a>
        </div>  
        <div style="margin: auto; width:60%; border:2px solid grey; text-align:center; margin-top: 80px">
            <h3>Order Information</h3>
            <form action="RestaurantOrderMenu.php" method="post">
                <p><label><b>Order Type:</b></label> 
                    <input type="radio" name="ordertype" value="delivery" required />Delivery
                    <input type="radio" name="ordertype" value="pickup">Pick Up
                </p>  
                <p><label><b>Enter Zip code:</b></label> 
                    <input type="text" name="zipcode" required>
                </p>
                <p><input type="submit" value="NEXT" style="width: 80px; height: 30px"></p>
            </form>
        </div>
    </body>
</html>

