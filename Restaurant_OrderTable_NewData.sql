Use Restaurantproject;
SET AUTOCOMMIT=0;
insert into FoodMenu (FoodName, Food_Description, Price) values ('House Roasted Turkey', 'Contain with Bacon, Cheddar, avacado, Lettuce and Tomato', '$5.00'),
       ('Chicken Sandwich', 'Contain Chicken, Veggies, BBQ Sauce with Bread', '$6.00'),
       ('Classic Caesar Salad', 'Served with Homemade Garlic Croutons', '$7.00'),
       ('Spinach Salad', 'Served with Crumbled Bacon and an orange vinaigrette', '$8.00'),
       ('Diet Coke', '20oz bottle', '$1.99'),
       ('Lemonade Bottle', '20oz bottle', '$1.99');
commit;

Use Restaurantproject;
SET AUTOCOMMIT=0;
insert into DeliveryPerson (DeliveryPersonName, Contact_Info, Assigned_AreaCode) values ('Mark', '6789032789', '19401'),
       ('Jill', '5678930927', '19402'),
       ('Henry', '4568397390', '19403'),
       ('Ram', '8976540932', '19404'),
       ('Alice', '6784047383', '19405'),
       ('Manne', '6109476387', '19406');
commit;

use restaurantproject;
SET AUTOCOMMIT=0;
insert into registration(first_name, last_name, email_id, password, gender) value ('Nancy', 'Botwin', 'nancybotwin@gmail.com', 'hello66', 'female');
commit;



-- Insert statements for ordertable table. This is the main table of restaurant project.
SET AUTOCOMMIT=0;
INSERT INTO Order
VALUES ('Pooja', 'Gagrani', '6789032789', 'poojagagrani09@gmail.com', 'avon road, 19403',1,1, '$11', '2', 'Pooja Gagrani', '2336792672937234', '03/20'),
       (1,'Pooja', 'Gagrani', '6789032789', 'poojagagrani09@gmail.com', 'avon road, 19403',3,2, '$28', '4', 'Pooja Gagrani', '2336792672937234', '03/20'),
       (1,'Pooja', 'Gagrani', '6789032789', 'poojagagrani09@gmail.com', 'avon road, 19403',1,2, '$20', '4', 'Pooja Gagrani', '2336792672937234', '03/20'),
       (2,'Navdeep', 'kaur', '7849839876', 'navkaur@gmail.com', 'mill road, pa',2,3, '$6', '1', 'Nav Kaur', '6768902739372347', '04/21'),
       (3,'Nav', 'deep', '76769839876', 'navdeepkaur@gmail.com', 'mill pike, pa',2,1, '$12', '2', 'Nav Deep', '6738397739378975', '05/22'),
       (4,'Deepika', 'Naga', '89676989876', 'deepikakalluri@hotmail.com', 'wilmu rd, pa',3,1, '$14', '2', 'Deepika Kalluri', '897839773937', '01/21'),
       (5,'sushrusha', 'Bezugam', '89769839876', 'sushrub@gmail.com', 'cardin pike, pa',1,3, '$20', '6', 'Sushrusha Bezugam', '903839773937', '07/22'),
       (5,'sushrusha', 'Bezugam', '89769839876', 'sushrub@gmail.com', 'cardin pike, pa',5,3, '$3', '2', 'Sushrusha Bezugam', '903839773937', '07/22'),
       (6,'Padmini', 'Sirandasu', '89769839896', 'padmini@hotmail.com', 'eagle rd, pa',5,3, '$6', '4', 'Padmini Sirandasu', '893839773937', '01/22'),
       (7,'Pavani', 'Jagarlamudi', '67676989876', 'pavanijagarla@gmail.com', 'exton pike, pa',1,1, '$10', '2', 'Pavani Jagarlamudi', '987839773937', '06/23'),
       (7,'Pavani', 'Jagarlamudi', '67676989876', 'pavanijagarla@gmail.com', 'exton pike, pa',3,1, '$7', '1', 'Pavani Jagarlamudi', '987839773937', '06/23'),
       (8,'Pav', 'Jagarmudi', '89676989876', 'pavijagarla@gmail.com', 'exton rd, pa',3,2, '$14', '2', 'Pavani Jagarlamudi', '427839773937', '07/23'),
       (9,'Ram', 'Jagarlamudi', '78676989876', 'ramjagarla@hotmail.com', 'wilmu rd, pa',3,1, '$14', '2', 'Ram Jagarlamudi', '897839773937', '01/21'),
       (9,'Ram', 'Jagarlamudi', '78676989876', 'ramjagarla@hotmail.com', 'wilmu rd, pa',4,1, '$16', '2', 'Ram Jagarlamudi', '897839773937', '01/21'),
       (10,'Rama', 'mudi', '90676989876', 'callmerama@gmail.com', 'Scrutz rd, pa',3,1, '$7', '1', 'Rama Mudi', '907839773937', '08/24'),
       (11,'Sunanda', 'Shetty', '89776989876', 'nandasu@gmail.com', 'chester rd, pa',3,1, '$7', '1', 'Sunanda Shetty', '897839773937', '09/21'),
       (11,'Sunanda', 'Shetty', '89776989876', 'nandasu@gmail.com', 'chester rd, pa',5,1, '$6', '4', 'Sunanda Shetty', '897839773937', '09/21'),
	(11,'Sunanda', 'Shetty', '89776989876', 'nandasu@gmail.com', 'chester rd, pa',1,1, '$5', '1', 'Sunanda Shetty', '897839773937', '09/21'),
       (12,'Prasad', 'Gagrani', '34676989876', 'prasadgagrani@hotmail.com', 'newward rd, pa',4,3, '$24', '3', 'Prasad Gagrani', '907839773937', '07/20'),
	(12,'Prasad', 'Gagrani', '34676989876', 'prasadgagrani@hotmail.com', 'newward rd, pa',4,5, '$3', '2', 'Prasad Gagrani', '907839773937', '07/20'),
	(12,'Prasad', 'Gagrani', '34676989876', 'prasadgagrani@hotmail.com', 'newward rd, pa',4,6, '$3', '2', 'Prasad Gagrani', '907839773937', '07/20'),
	(13,'Manasa', 'Rai', '97769839876', 'manasarai@gmail.com', 'vonn pike, pa',5,3, '$3', '2', 'Manasa Rai', '233839773937', '04/22'),
	(14,'Lavanya', 'Chowdary', '78769839876', 'lavc@gmail.com', 'devon pike, pa',5,3, '$10', '5', 'Lavanya Chowdary', '233899773937', '09/23'),
	(14,'Lavanya', 'Chowdary', '78769839876', 'lavc@gmail.com', 'devon pike, pa',6,3, '$10', '5', 'Lavanya Chowdary', '233899773937', '09/23'),
	(14,'Lavanya', 'Chowdary', '78769839876', 'lavc@gmail.com', 'devon pike, pa',1,3, '$25', '5', 'Lavanya Chowdary', '233899773937', '09/23'),
	(15,'Alekya', 'Reddy', '90769839876', 'alekya@gmail.com', 'devon rd, pa',3,3, '$35', '5', 'Alekya Reddy', '903899773937', '01/22'),
	(15,'Alekya', 'Reddy', '90769839876', 'alekya@gmail.com', 'devon rd, pa',6,3, '$10', '5', 'Alekya Reddy', '903899773937', '01/22'),
	(15,'Alekya', 'Reddy', '90769839876', 'alekya@gmail.com', 'devon rd, pa',1,3, '$5', '1', 'Alekya Reddy', '903899773937', '01/22'),
	(16,'Keerti', 'Atluri', '56789839876', 'keerti@hotmail.com', 'keepon rd, pa',3,6, '$35', '5', 'keerti Atluri', '483899773937', '09/22'),
	(16,'Keerti', 'Atluri', '56789839876', 'keerti@hotmail.com', 'keepon rd, pa',1,6, '$25', '5', 'keerti Atluri', '483899773937', '09/22'),
	(16,'Keerti', 'Atluri', '56789839876', 'keerti@hotmail.com', 'keepon rd, pa',4,6, '$16', '2', 'keerti Atluri', '483899773937', '09/22'),
	(17,'Neeha', 'Dev', '78789839876', 'neehadev@gmail.com', 'avon rd, pa',2,3, '$12', '2', 'Neeha Dev', '893899773937', '02/20'),
	(17,'Neeha', 'Dev', '78789839876', 'neehadev@gmail.com', 'avon rd, pa',1,3, '$25', '5', 'Neeha Dev', '893899773937', '02/20'),
	(18,'Deevika', 'Rao', '67789839876', 'devikadeo@gmail.com', 'sugartown rd, pa',2,3, '$12', '2', 'Deevika Deo', '893899773937', '02/20'),
	(18,'Deevika', 'Rao', '67789839876', 'devikadeo@gmail.com', 'sugartown rd, pa',1,3, '$5', '1', 'Deevika Deo', '893899773937', '02/20'),
	(19,'Rakhi', 'Gagrani', '89789839876', 'raksan@gmail.com', 'exton rd, pa',1,5, '$5', '1', 'Rakhi Gagrani', '890899573937', '06/21'),
	(19,'Rakhi', 'Gagrani', '89789839876', 'raksan@gmail.com', 'exton rd, pa',2,5, '$24', '4', 'Rakhi Gagrani', '890899573937', '06/21'),
	(19,'Rakhi', 'Gagrani', '89789839876', 'raksan@gmail.com', 'exton rd, pa',6,5, '$1.99', '1', 'Rakhi Gagrani', '890899573937', '06/21'),
	(20,'Rakhi', 'Baldawa', '89789839876', 'rakhibaldawa@gmail.com', 'Acme rd, pa',6,2, '$6', '3', 'Rakhi Baldawa', '780899573937', '09/19'),
	(20,'Rakhi', 'Baldawa', '89789839876', 'rakhibaldawa@gmail.com', 'Acme rd, pa',1,2, '$15', '3', 'Rakhi Baldawa', '780899573937', '09/19'),
	(20,'Rakhi', 'Baldawa', '89789839876', 'rakhibaldawa@gmail.com', 'Acme rd, pa',2,2, '$18', '3', 'Rakhi Baldawa', '780899573937', '09/19'),
	(21,'Shriya', 'Chopra', '56989839876', 'shriya@gmail.com', 'western rd, pa',6,4, '$6', '3', 'Shriya Chopra', '909999573937', '12/19'),
	(21,'Shriya', 'Chopra', '56989839876', 'shriya@gmail.com', 'western rd, pa',5,4, '$6', '3', 'Shriya Chopra', '909999573937', '12/19'),
	(21,'Shriya', 'Chopra', '56989839876', 'shriya@gmail.com', 'western rd, pa',3,4, '$21', '3', 'Shriya Chopra', '909999573937', '12/19'),
	(22,'Shruti', 'Chopda', '56982322876', 'shrutichopda@gmail.com', 'eastern rd, pa',6,1, '$6', '3', 'Shruti Chopda', '899999573937', '12/20'),
	(23,'Chayya', 'Bhaiya', '76889839876', 'chayya@hotmail.com', 'pugh rd, pa',2,2, '$6', '1', 'Chayya Bhaiya', '874999573937', '12/21'),
	(23,'Chayya', 'Bhaiya', '76889839876', 'chayya@hotmail.com', 'pugh rd, pa',4,2, '$32', '4', 'Chayya Bhaiya', '874999573937', '12/21'),
	(24,'Stuti', 'Bhaiya', '24539839876', 'stuti.bhaiya@gmail.com', 'pugh rd, pa',2,2, '$9', '5', 'Stuti Bhaiya', '90876583937', '09/19'),
	(24,'Stuti', 'Bhaiya', '24539839876', 'stuti.bhaiya@gmail.com', 'pugh rd, pa',4,2, '$8', '5', 'Stuti Bhaiya', '90876583937', '09/19'),
	(24,'Stuti', 'Bhaiya', '24539839876', 'stuti.bhaiya@gmail.com', 'pugh rd, pa',1,2, '$25', '5', 'Stuti Bhaiya', '90876583937', '09/19'),
	(25,'Aditi', 'Rai', '90889839876', 'aditi.rai@hotmail.com', 'stonegate pike, pa',2,2, '$12', '2', 'Aditi Rai', '765899573937', '12/22');
	COMMIT;

