<html>
    <head>
        <title>Order Menu</title>
        <style>
            body {
                /* background-image: url("close-up-1854245_1920.jpg"); */
                background-color: whitesmoke;  /*DCDCDC*/
                background-position: center; /* Center the image */
                background-repeat: no-repeat; /* Do not repeat the image */
                background-size: cover;
            }
            .topnav {
                overflow: hidden;
                background-color: #39d07f; /* f3f3f3 009900*/
            }
            .topnav a {
                float: left;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }
            .topnav a:hover {
                text-decoration: underline;
            }
            .topnav a.active {
                background-color: #4CAF50;
                color: white;
            }
            .quantity, .total {
                text-align: center;
            }
            input[type=text], input[type=email] {
                padding-left: 10px;
                width: 250px;
                height: 23px;
            }
            .required {
                color: red;
            }
            .restaurantname {
                font-size: 50px; 
                color:#cc0033; 
                margin-left: 70px; 
                margin-top: 50px;
            }
            .menucategory {
                font-size:20px; 
                background-color: #ccffcc;
            }
            .ordertable {
                width:60%; 
                margin: auto;
            }
        </style>      
    </head>
    <body>
        
        <div class="topnav" style="width:90%; margin: auto;">
            <a href="/PhpProjectLessons/RestaurantHomePage.php"><img src="LogoOne.png" alt="Restaurant Logo" height="60" width="60"></a>
            <a href="/PhpProjectLessons/RestaurantOrderMenu.php" style="float:right">Order Now</a>
            <a href="#ContactUs" style="float:right">Contact Us</a>
            <a href="/PhpProjectLessons/RestaurantRegistration.php" style="float:right">Sign Up</a>
            <a href="/PhpProjectLessons/LoginPage.php" style="float:right">Login</a>
            <a style="text-decoration: none;" href="/PhpProjectLessons/RestaurantHomePage.php"><b class="restaurantname">Navja Restaurant</b></a>
        </div>
        <div style="margin: auto; width:60%; border:2px solid grey; text-align:center; margin-top: 10px">
            <form action="RestaurantOrderMenu.php" method="post">
                <div class="menu">
                    <h3>Order Menu</h3>
                    <table class="ordertable">
                        <tr>
                            <th></th>
                            <th>Price</th>
                            <th class="quantity">Quantity</th>
                            <th>Amount</th>
                        </tr>
                        <tr class="menucategory">
                            <td colspan="4"><strong>Sandwiches</strong></td>
                        </tr>
                        <tr>
                            <td><b>House Roasted Turkey</b><br>Contain with bacon,<br> cheddar, avocado, lettuce,<br> and tomato</td>
                            <td><input type="text" name="hrtprice" id="hrtprice" value="$5.00" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=sandwich_HRT id="sandwichHRTquan" onchange="calc('hrtprice','sandwichHRTquan','hrttotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="hrttotalprice" id="hrttotalprice" disabled="true" style="width:50px"></td>
                        </tr><br>
                        <tr>
                            <td><b>Chicken Sandwich</b><br>Contain savory chicken,<br> veggies, barbeque sauce<br> and seasoning in freshly<br> baked bread</td>
                            <td><input type="text" name="csprice" id="csprice" value="$6.00" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=sandwich_CS id="sandwichCSquan" onchange="calc('csprice','sandwichCSquan','csttotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="cstotalprice" id="csttotalprice" disabled="true" style="width:50px"></td>
                        </tr>
                        <tr class="menucategory"><td colspan="4"><strong>Salads</strong></td></tr>
                        <tr>
                            <td><b>Classic Caesar Salad</b><br>Classic caesar served<br> with our house made<br> garlic croutons</td>
                            <td><input type="text" name="ccsprice" id="ccsprice" value="$7.00" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=salad_CCS id="saladCCSquan" onchange="calc('ccsprice','saladCCSquan','ccsttotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="ccstotalprice" id="ccsttotalprice" disabled="true" style="width:50px"></td>
                        </tr>
                        <tr>
                            <td><b>Spinach Salad</b><br>Served with crumbled<br> bacon and an orange<br> vinaigrette</td>
                            <td><input type="text" name="ssprice" id="ssprice" value="$8.00" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=salad_SS id="saladSSquan" onchange="calc('ssprice','saladSSquan','ssttotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="sstotalprice" id="ssttotalprice" disabled="true" style="width:50px"></td>
                        </tr>
                        <tr class="menucategory"><td colspan="4"><strong>Beverages</strong></td></tr>
                        <tr>
                            <td><b>Diet Coke</b><br>20oz bottle <br>  balanced adult cola<br> taste in a no calorie<br> beverage</td>
                            <td><input type="text" name="cokeprice" id="cokeprice" value="$1.99" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=beveragecoke id="beveragecokequan" onchange="calc('cokeprice','beveragecokequan','coketotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="coketotalprice" id="coketotalprice" disabled="true" style="width:50px"></td>
                        </tr>
                        <tr>
                            <td><b>Lemonade Bottle</b><br>20oz Bottle<br> Minute Maid Lemonade<br> is the quintessential<br> refreshing beverage</td>
                            <td><input type="text" name="lemonadeprice" id="lemonadeprice" value="$1.99" disabled="true" style="width:50px"></td>
                            <td class="quantity">
                                <select name=beveragelemon id="beveragelemonquan" onchange="calc('lemonadeprice','beveragelemonquan','lemonadetotalprice'); calctotalPrice();">
                                    <option value=0>0</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </td>
                            <td class="total"><input type="text" name="lemonadetotalprice" id="lemonadetotalprice" disabled="true" style="width:50px"></td>
                        </tr>
                        <tr></tr>
                        <tr style="background-color: #cccccc;">
                            <td colspan="2" style="font-size:20px; "><b>Total</b></td>
                            <td colspan="2" style="text-align:right"><b><input type="text" name="totalprice" id="totalprice" disabled="true" style="width:50px"></b></td></tr>
                    </table>
                </div>
                <div>
                    <h3 >Contact Details</h3>
                    <table class="ordertable">
                        <tr>
                            <td>First Name:<span class="required">*</span></td>
                            <td><input type="text" name="FirstName" required>
                        </tr>
                        <tr>
                            <td>Last Name:<span class="required">*</span></td>
                            <td><input type="text" name="LasttName" required>
                        </tr>
                        <tr>
                            <td>Phone Number:<span class="required">*</span></td>
                            <td><input type="text" name="PhoneNumber" required>
                        </tr>
                        <tr>
                            <td>Email Address:<span class="required">*</span></td>
                            <td><input type="email" name="EmailAddress" required>
                        </tr>
                        <tr>
                            <td>Zip Code:<span class="required">*</span></td>
                            <td><input type="text" name="zipcode" required>
                        </tr>
                        <tr>
                            <td>Delivery Address:<span class="required">*</span></td>
                            <td><textarea rows="4" cols="33"></textarea></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <h3>Payment Information</h3>
                    <table class="ordertable">
                        <tr>
                            <td>Name on Card:<span class="required">*</span></td>
                            <td><input type="text" name="CardName" required>
                        </tr>
                        <tr>
                            <td>Card Number:<span class="required">*</span></td>
                            <td><input type="text" name="CardNumber" required>
                        </tr>
                        <tr>
                            <td>Expiration Date:<span class="required">*</span></td>
                            <td>
                                <select name=expirymonth>
                                    <option value=month>Month</option>
                                    <option value=01>01</option>
                                    <option value=02>02</option>
                                    <option value=03>03</option>
                                    <option value=04>04</option>
                                    <option value=05>05</option>
                                    <option value=06>06</option>
                                    <option value=07>07</option>
                                    <option value=08>08</option>
                                    <option value=09>09</option>
                                    <option value=10>10</option>
                                    <option value=11>11</option>
                                    <option value=12>12</option>
                                </select>
                                <select name=expiryyear style="margin-left: 40px;">
                                    <option value=month>Year</option>
                                    <option value=2019>2019</option>
                                    <option value=2020>2020</option>
                                    <option value=2021>2021</option>
                                    <option value=2022>2022</option>
                                    <option value=2023>2023</option>
                                    <option value=2024>2024</option>
                                    <option value=2025>2025</option>
                                    <option value=2026>2026</option>
                                    <option value=2027>2027</option>
                                    <option value=2028>2028</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Security Code:<span class="required">*</span></td>
                            <td><input type="text" name="SecurityCode" required>
                        </tr>
                    </table>
                </div>
                <p><input type="submit" value="Place Your Order" style="width: 120px; height: 30px"></p>
            </form>
        </div>
        <script>
            function calc(priceId, quantityId, totalpriceId) {
            var price = document.getElementById(priceId).value;
            //console.log(priceId);
            var number = Number(price.replace(/[^0-9\.]+/g,""));
//            document.write(price);
            var e = document.getElementById(quantityId);
            var quantity = e.options[e.selectedIndex].text; 
            var totalpriceofitem = (number*quantity).toFixed(2);
            document.getElementById(totalpriceId).value = totalpriceofitem;
            }
            
            function calctotalPrice() {
                var hrtSandwichTotalPrice = document.getElementById("hrttotalprice").value;
                var csSandwichTotalPrice = document.getElementById("csttotalprice").value;
                var ccsSaladTotalPrice = document.getElementById("ccsttotalprice").value;
                var ssSaladTotalPrice = document.getElementById("ssttotalprice").value;
                var cokeTotalPrice = document.getElementById("coketotalprice").value;
                var lemonadeTotalPrice = document.getElementById("lemonadetotalprice").value;
                var totalPrice = (+hrtSandwichTotalPrice + +csSandwichTotalPrice + +ccsSaladTotalPrice + 
                        +ssSaladTotalPrice + +cokeTotalPrice + +lemonadeTotalPrice).toFixed(2);;
                document.getElementById("totalprice").value = totalPrice;
            }
        </script>
    </body>
</html>